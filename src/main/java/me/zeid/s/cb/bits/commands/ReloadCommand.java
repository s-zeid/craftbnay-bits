/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.commands;

import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import me.zeid.s.cb.bits.BITS;
import me.zeid.s.cb.config.CMPlugin;
import me.zeid.s.cb.config.ConfigManager;

public class ReloadCommand extends BITSCommand {
 public static boolean
 run(CMPlugin plugin, CommandSender sender, Command cmd, String label, String[] args) {
  if (!sender.hasPermission("cb-bits.reload")) {
   sendError(sender, "You do not have permission to reload the BITS plugin configuration");
   return true;
  }
  
  ConfigManager bitsConfig = plugin.getConfigManager();
  String[] categories = { null };
  if (args.length > 0)
   categories = args;
  int succeeded = 0;
  for (String category : categories) {
   try {
    if (category == null) {
     bitsConfig.reload();
     succeeded += 1;
     break;
    } else {
     category = category.trim().toLowerCase();
     if (bitsConfig.isCategory(category)) {
      bitsConfig.reload(category);
      succeeded += 1;
     } else
      sendError(sender, "\"" + category + "\" is not a known category.");
    }
   } catch (Exception e) {
    String what  = (category != null) ? " category \"" + category + "\"" : "";
    String error = "Error reloading BITS configuration" + what + ".";
    sendError(sender, error + "  See server console for details.");
    plugin.getLogger().log(Level.SEVERE, error, e);
   }
  }
  
  if (succeeded == categories.length)
   sendSuccess(sender, "Configuration reloaded.");
  else if (succeeded > 0)
   sendWarning(sender, "Configuration partially reloaded.");
  else
   sendError(sender, "Configuration NOT reloaded.");
  
  return true;
 }
}
