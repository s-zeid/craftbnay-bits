/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import me.zeid.s.cb.bits.BITS;
import me.zeid.s.cb.bits.ridetasks.*;
import me.zeid.s.cb.ridetasks.RideTask;
import me.zeid.s.cb.ridetasks.RideTaskManager;

public class RideTaskCommand extends BITSCommand {
 public static boolean
 run(BITS plugin, CommandSender sender, Command cmd, String label, String[] args) {
  if (!(sender instanceof Player)) {
   sendError(sender, "Only players can use this command");
   return true;
  }
  
  if (args.length < 1)
   return false;
  
  String taskName = args[0].trim().toLowerCase();
  args = Arrays.copyOfRange(args, 1, args.length);
  
  if (!sender.hasPermission("cb-bits.ridetask")) {
   sendError(sender, "You do not have permission to run ride tasks");
   return true;
  }
  
  Player          player     = (Player) sender;
  RideTaskManager tasks      = plugin.getRideTaskManager();
  RideTask        activeTask = tasks.getRideTask(player);
  if (activeTask == null) {
   if (!sender.hasPermission("cb-bits.ridetask." + taskName) &&
       !sender.hasPermission("cb-bits.ridetask.*")) {
    sendError(sender, "You do not have permission to run the"
                     +" \"" + taskName + "\" ride task");
    return true;
   }
   else if (taskName.equals("test"))
    activeTask = new TestRideTask(player, plugin);
   else {
    sendError(sender, "\"" + taskName + "\" is not a known task.");
    return true;
   }
   tasks.setRideTask(player, activeTask);
   sendSuccess(sender, "You are now running the \"" + taskName + "\" ride task.");
   sendSuccess(sender, "To stop the task, run `/" + label + " ridetask stop`"
                      +" (or \"end\" or \"cancel\").");
  } else if (taskName.equals("stop")||taskName.equals("end")||taskName.equals("cancel")) {
   activeTask.stop();
   tasks.removeRideTask((Player) sender);
   sendSuccess(sender, "Your ride task has been stopped.");
  } else {
   sendError(sender, "You are already running a ride task.  Cancel it first.");
  }
  
  return true;
 }
}
