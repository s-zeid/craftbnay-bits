/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.config;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import me.zeid.s.cb.config.ConfigManager;
import me.zeid.s.cb.config.ConfigManagerException;

public class BITSConfig extends ConfigManager {
 public BITSConfig(Plugin plugin) throws ConfigManagerException {
  super(new String[] { "bits", "sounds", "players" }, plugin);
 }
 protected void instantiateCategory(String category) throws ConfigManagerException {
  if (category.equals("bits")) {
   this.load(category, new YamlConfiguration(), "bits.yml");
  } // bits
  if (category.equals("sounds")) {
   this.load(category, new SoundListConfiguration(), "sounds");
  } // sounds
  if (category.equals("players")) {
   if (this.get("bits") == null) this.load("bits");
   this.load(category, new PlayerConfiguration(), "players",
             this.get("bits", "player-defaults"));
  } // players
 }
}
