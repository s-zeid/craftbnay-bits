/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.listeners;

import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class VehicleExitListener implements Listener {
 private static boolean checkSide(Block block, Block block2, BlockFace side) {
  return (isWall(block.getRelative(side, 1), block2.getRelative(side, 1)) &&
          !block2.getRelative(side.getOppositeFace(), 1).getType().isSolid());
 }
 
 private static boolean isWall(Block block, Block block2) {
  return (isWallBlock(block, false) && isWallBlock(block2, true));
 }
 
 private static boolean isWallBlock(Block block, boolean includeFenceBelow) {
  if (block.getType().isSolid())
   return true;
  if (includeFenceBelow && block.getLocation().getBlockY() > 0) {
   switch (block.getRelative(BlockFace.DOWN, 1).getType()) {
    case FENCE:
    case FENCE_GATE:
    case COBBLE_WALL:
     return true;
   }
  }
  return false;
 }

 @EventHandler(priority=EventPriority.HIGHEST)
 public void onVehicleExit(VehicleExitEvent e) {
  Location src = e.getVehicle().getLocation();
  Location dst = null;
  Block block = src.getBlock();
  Block block2 = block;
  if (src.getBlockZ() + 1 < src.getWorld().getMaxHeight())
   block2 = block.getRelative(BlockFace.UP, 1);
  
  if (false) {}
  
  else if (this.checkSide(block, block2, BlockFace.NORTH)) {
   dst = block2.getRelative(BlockFace.SOUTH, 1).getLocation();
   dst.setYaw(180);  // north
  }
  
  else if (this.checkSide(block, block2, BlockFace.SOUTH)) {
   dst = block2.getRelative(BlockFace.NORTH, 1).getLocation();
   dst.setYaw(0);  // south
  }
  
  else if (this.checkSide(block, block2, BlockFace.EAST)) {
   dst = block2.getRelative(BlockFace.WEST, 1).getLocation();
   dst.setYaw(-90);  // east
  }
  
  else if (this.checkSide(block, block2, BlockFace.WEST)) {
   dst = block2.getRelative(BlockFace.EAST, 1).getLocation();
   dst.setYaw(90);  // west
  }
  
  if (dst == null)
   return;
  
  dst.setPitch(45);
  dst.add(0.5, 0, 0.5);
  
  class TeleportTask extends TimerTask {
   private LivingEntity exited;
   private Location     dst;
   TeleportTask(LivingEntity exited, Location dst) {
    this.exited = exited;
    this.dst    = dst;
   }
   public void run() {
    exited.teleport(dst);
   }
  }
  
  Timer timer = new Timer();
  timer.schedule(new TeleportTask(e.getExited(), dst), 50);
 }
}
