/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.commands;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.zeid.s.cb.bits.BITS;
import me.zeid.s.cb.soundspecs.SoundSpec;

public class AnnounceCommand extends BITSCommand {
 public static boolean
 run(BITS plugin, CommandSender sender, Command cmd, String label, String[] args) {
  if (args.length < 1)
   return false;
  if (!(sender instanceof Player) && args.length < 2)
   return false;
  
  String soundSpec  = args[0];
  String playToName = (args.length > 1) ? args[1] : sender.getName();
  Player playTo     = plugin.getServer().getPlayer(playToName);
  
  if (!sender.hasPermission("cb-bits.announce.others") && !sender.equals(playTo)) {
   sendError(sender, "You do not have permission to make BITS announcements to other"
                    +" players");
   return true;
  }
  if (!sender.hasPermission("cb-bits.announce")) {
   sendError(sender, "You do not have permission to make BITS announcements");
   return true;
  }
  
  if (playTo == null || !playTo.isOnline()) {
   if (plugin.getServer().getOfflinePlayer(playToName).hasPlayedBefore())
    sendError(sender, "The player " + playToName + " does not exist");
   else
    sendError(sender, "The player " + playToName + " is offline");
   return true;
  }
  
  ConfigurationSection playerConfig = plugin.getPlayerConfig(playToName);
  boolean sendSound = playerConfig.getBoolean("sound");
  boolean sendText  = playerConfig.getBoolean("text");
  
  if (!sendSound && !sendText)
   return true;
  
  List<String> playSoundArgs = new ArrayList<String>();
  if (args.length > 2) {
   Location loc = null;
   if (sender instanceof Player)
    loc = ((Player) sender).getLocation();
   if (sender instanceof BlockCommandSender)
    loc = ((BlockCommandSender) sender).getBlock().getLocation();
   for (int i = 2; i < args.length; i++) {
    String arg = args[i];
    if (arg.startsWith("~") && loc != null) {
     Long offset = tildeStringToLong(arg);
     if (offset != null) {
      switch (i) {
       case 2: arg = String.valueOf(loc.getX() + offset); break;
       case 3: arg = String.valueOf(loc.getY() + offset); break;
       case 4: arg = String.valueOf(loc.getZ() + offset); break;
      }
     }
    }
    playSoundArgs.add(arg);
   }
  }
  
  List<String> searchPath = plugin.getConfig("bits").getStringList("sounds.search-path");
  SoundSpec spec = new SoundSpec(soundSpec, plugin.getConfig("sounds"), searchPath);
  spec.sendSound(sendSound).sendText(sendText).playTo(playTo, playSoundArgs);
  return true;
 }
}
