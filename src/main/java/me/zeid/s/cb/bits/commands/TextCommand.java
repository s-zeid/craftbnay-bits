/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.zeid.s.cb.bits.BITS;

public class TextCommand extends BITSCommand {
 public static boolean
 run(BITS plugin, CommandSender sender, Command cmd, String label, String[] args) {
  if (!(sender instanceof Player)) {
   sendError(sender, "Only players can use this command");
   return true;
  }
  
  if (!sender.hasPermission("cb-bits.text")) {
   sendError(sender, "You do not have permission to change your BITS text announcement"
                    +" settings");
   return true;
  }
  
  String  playerName = (args.length > 1) ? args[1] : sender.getName();
  boolean set = (args.length > 0 && !args[0].equals("?") && !args[0].equals("get"));
  String  action = (set) ? "set" : "get";

  if (!sender.getName().equals(playerName) &&
      !sender.hasPermission("cb-bits.text.others." + action)) {
   sendError(sender, "You do not have permission to " + action
                    +" other players' BITS text announcement settings");
   return true;
  }
  
  ConfigurationSection player = plugin.getPlayerConfig(playerName);
  String key = "text";
  if (set) {
   // Set sound on/off
   player.set(key, stringToBoolean(args[0].toLowerCase()));
   plugin.saveConfig("players", playerName);
  }
  // Get sound on/off
  String who     = (sender.getName().equals(playerName)) ? "You are" : playerName + " is";
  String adverb  = (set) ? "now" : "currently";
  String which   = (player.getBoolean(key)) ? "to" : "to not";
  String message = who+" "+adverb+" set "+which+" receive BITS text announcements";
  if (set)
   sendSuccess(sender, message);
  else
   sendInfo(sender, message);
  
  return true;
 }
}
