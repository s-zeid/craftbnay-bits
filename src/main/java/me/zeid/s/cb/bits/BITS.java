/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.bits;

import java.util.Arrays;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.HandlerList;
 
import me.zeid.s.cb.bits.config.*;
import me.zeid.s.cb.bits.commands.*;
import me.zeid.s.cb.bits.listeners.*;
import me.zeid.s.cb.config.ConfigManagerPlugin;
import me.zeid.s.cb.ridetasks.RideTask;
import me.zeid.s.cb.ridetasks.RideTaskManager;

public final class BITS extends ConfigManagerPlugin {
 private RideTaskManager rideTaskManager = null;
 
 public BITS() {
  super(BITSConfig.class);
 }
 
 public ConfigurationSection getPlayerConfig(String name) {
  return this.getConfig("players", name);
 }
 
 public RideTaskManager getRideTaskManager() {
  return this.rideTaskManager;
 }
 
 @Override
 public void onEnable() {
  if (!this.getDataFolder().exists())
   this.saveResource("sounds/example.yml.txt", false);
  
  this.rideTaskManager = new RideTaskManager(this);
  
  getServer().getPluginManager().registerEvents(new VehicleExitListener(), this);
 }
 
 @Override
 public void onDisable() {
  HandlerList.unregisterAll(this);
 }
 
 @Override
 public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
  if (cmd.getName().equalsIgnoreCase("bits")) {
   if (args.length < 1)
    return false;
   String subcommand = args[0];
   String[] subcommand_args = Arrays.copyOfRange(args, 1, args.length);
   if (subcommand.equalsIgnoreCase("announce"))
    return AnnounceCommand.run(this, sender, cmd, label, subcommand_args);
   if (subcommand.equalsIgnoreCase("reload"))
    return ReloadCommand.run(this, sender, cmd, label, subcommand_args);
   if (subcommand.equalsIgnoreCase("ridetask"))
    return RideTaskCommand.run(this, sender, cmd, label, subcommand_args);
   if (subcommand.equalsIgnoreCase("sound"))
    return SoundCommand.run(this, sender, cmd, label, subcommand_args);
   if (subcommand.equalsIgnoreCase("text"))
    return TextCommand.run(this, sender, cmd, label, subcommand_args);
  }
  return false;
 }
}
