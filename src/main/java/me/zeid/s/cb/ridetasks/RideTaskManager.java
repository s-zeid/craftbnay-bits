/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.ridetasks;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class RideTaskManager {
 private Listener             listener;
 private Map<String,RideTask> rideTasks;
 private Plugin               plugin;
 
 public RideTaskManager(Plugin plugin) {
  this.rideTasks = new HashMap<String,RideTask>();
  this.plugin    = plugin;
  this.listener  = new Listener() {
   @EventHandler
   public void onPlayerQuit(PlayerQuitEvent e) {
    Player player = e.getPlayer();
    RideTask task = RideTaskManager.this.getRideTask(player);
    if (task != null) {
     task.onPlayerQuit(e);
     if (!task.isRunning())
      RideTaskManager.this.removeRideTask(player);
    }
   }
  };
  plugin.getServer().getPluginManager().registerEvents(listener, plugin);
 }
 
 public Plugin getPlugin() { return this.plugin; }
 
 public RideTask getRideTask(Player player) {
  return rideTasks.get(player.getName());
 }
 public boolean hasRideTask(Player player) {
  return rideTasks.containsKey(player.getName());
 }
 public void removeRideTask(Player player) {
  RideTask task = this.getRideTask(player);
  if (task != null)
   task.stop();
  rideTasks.remove(player.getName());
 }
 public void setRideTask(Player player, RideTask task) {
  rideTasks.put(player.getName(), task);
 }
}
