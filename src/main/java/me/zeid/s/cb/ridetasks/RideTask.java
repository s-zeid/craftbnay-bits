/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.ridetasks;

import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;
 
public abstract class RideTask {
 private Listener         extraListener;
 private boolean          floorLocation;
 private RideTaskListener mainListener;
 private Player           player;
 private Plugin           plugin;
 private boolean          running;
 
 public RideTask(Player player, Plugin plugin) {
  this(player, plugin, null);
 }
 
 public RideTask(Player player, Plugin plugin, Listener extraListener) {
  this.extraListener = extraListener;
  this.floorLocation = false;
  this.player        = player;
  this.plugin        = plugin;
  this.running       = true;
  
  this.mainListener  = new RideTaskListener(this);
  plugin.getServer().getPluginManager().registerEvents(mainListener, plugin);
  if (extraListener != null)
   plugin.getServer().getPluginManager().registerEvents(extraListener, plugin);
 }
 
 public Player  getPlayer() { return this.player; }
 public Plugin  getPlugin() { return this.plugin; }
 public boolean isRunning() { return this.running; }
 
 public boolean  floorLocation() { return this.floorLocation; }
 public RideTask floorLocation(boolean floorLocation) {
  this.floorLocation = floorLocation;
  return this;
 }
 
 public static Location floorLocation(Location loc) {
  return new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
 }
 public static String coords(Location loc) {
  String x = String.format(((loc.getX() == loc.getBlockX()) ? "%.0f" : "%.3f"),loc.getX());
  String y = String.format(((loc.getY() == loc.getBlockY()) ? "%.0f" : "%.3f"),loc.getY());
  String z = String.format(((loc.getZ() == loc.getBlockZ()) ? "%.0f" : "%.3f"),loc.getZ());
  return String.format("(%s, %s, %s)", x, y, z);
 }
 
 protected void onPlayerQuit(PlayerQuitEvent e) {
  this.stop();
 }
 protected void onStop() {}
 protected abstract void onVehicleMove(VehicleMoveEvent e);
 
 public void stop() {
  if (this.running) {
   this.running = false;
   HandlerList.unregisterAll(this.mainListener);
   if (this.extraListener != null) {
    HandlerList.unregisterAll(this.extraListener);
   }
   this.onStop();
  }
 }
 
 class RideTaskListener implements Listener {
  private RideableMinecart minecart;
  private RideTask         task;
  
  RideTaskListener(RideTask task) {
   this.task = task;
   this.setMinecart();
  }
  
  private void setMinecart() {
   this.setMinecart(this.task.getPlayer().getVehicle());
  }
  private void setMinecart(Entity entity) {
   if (entity != null && entity instanceof RideableMinecart)
    this.minecart = (RideableMinecart) entity;
   else
    this.minecart = null;
  }
  
  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent e) {
   if (e.getPlayer().equals(this.task.getPlayer()))
    this.task.onPlayerQuit(e);
  }
  
  @EventHandler
  public void onVehicleEnter(VehicleEnterEvent e) {
   if (e.getEntered().equals(this.task.getPlayer())) {
    this.setMinecart(e.getVehicle());
    e.getVehicle().setVelocity(new Vector(0, 0, 0));
   }
  }
  
  @EventHandler
  public void onVehicleExit(VehicleExitEvent e) {
   if (e.getVehicle().equals(this.minecart))
    this.setMinecart(null);
  }
  
  @EventHandler
  public void onVehicleMove(VehicleMoveEvent e) {
   if (e.getVehicle().equals(this.minecart)) {
    if (this.task.floorLocation())
     e = new VehicleMoveEvent(e.getVehicle(),
                              this.task.floorLocation(e.getFrom()),
                              this.task.floorLocation(e.getTo()));
    this.task.onVehicleMove(e);
   }
  }
 }
}
